from docx.shared import Cm
from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml
from docx import Document
from docx.shared import Inches, Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.style import WD_STYLE_TYPE
from setup_styles import *
from os import path, getcwd
import re, subprocess

level = 0   # Set a global level of numeric lists
workspace = getcwd()
img_path = path.join(workspace, 'img')
report_name = 'Adstetica_Report.docx'
everypixel_logo_path = path.join(img_path, 'Everypixel-logo.png')
rectangle_logo_path = path.join(img_path, 'purple_square.png')


def create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=0, align='JUSTIFY'):
    global level
    p = document.add_paragraph(text, style=style)
    if style == 'List Number':
        list_number(document, p, prev=prev, level=level, num=True)
    p.paragraph_format.space_before = Pt(before)
    p.paragraph_format.first_line_indent = Cm(indent)
    if align == 'JUSTIFY':
        p.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    elif align == 'DISTRIBUTE':
        p.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.DISTRIBUTE
    elif align == 'LEFT':
        p.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.LEFT
    p.paragraph_format.space_after = Pt(after)
    return p


document = Document()
# section = document.sections[0] # section = document.add_section(docx.enum.section.WD_SECTION.NEW_PAGE)

sections = document.sections
for section in sections:
    section.left_margin = Cm(2)
    section.right_margin = Cm(2)
    section.top_margin = Cm(0)
    section.bottom_margin = Cm(2)
styles = document.styles
setup_styles(styles)
style = document.styles['Normal']
font = style.font
font.name = font_style
font.size = Pt(font_size)

header = document.sections[0].header
htable = header.add_table(1, 1, width=Inches(30))
htab_cells = htable.rows[0].cells
logo_cell = htab_cells[0]
p = htab_cells[0].add_paragraph()
p.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.RIGHT
p.paragraph_format.space_before = Pt(0)
p.paragraph_format.space_after = Pt(0)
cell = p.add_run()
cell.add_picture(everypixel_logo_path, width=Inches(2))
cell.add_picture(rectangle_logo_path, width=Cm(1.2))
set_cell_border(
        logo_cell,
        top={"sz": 0, "val": "single", "color": "#FFFFFF", "space": "0"},
        bottom={"sz": 0, "color": "#FFFFFF", "val": "single", "space": "0"},
        start={"sz": 0, "color": "#FFFFFF", "val": "single", "space": "0"},
        end={"sz": 0, "color": "#FFFFFF", "val": "single", "space": "2.5"},
    )

p = create_paragraph(text='', style='Normal', before=0, after=12, prev=None, indent=0, align='JUSTIFY')
p.add_run('Черный и ', style='BlackBoldStyleFont')
p.add_run('фиолетовый ', style='PurpleBoldStyleFont')
p.add_run('жирные шрифты ', style='BlackBoldStyleFont')
p.add_run(u"- это постоянная часть отчета, она в нем всегда (фиолетовый используется только для визуального выделения ключевых пунктов в отчете для клиента).", style = 'BlackStyleFont')
p.add_run("\nКрасный шрифт", style='RedStyleFont')
p.add_run(u" - это информация из брифа клиента.", style='BlackStyleFont')
p.add_run('\nЗеленый шрифт ', style='GreenStyleFont')
p.add_run(u'- это результат работы скоринга, берется из статистики по группам', style='BlackStyleFont')
p.add_run("\nСиний шрифт ", style='BlueStyleFont')
p.add_run("- это наша база шаблонов и описаний под каждый психотип.", style='BlackStyleFont')

table = document.add_table(1, 1)
table_cells = table.rows[0].cells
main_cell = table_cells[0]
table.style = 'Table Grid'
p0 = table.cell(0, 0).paragraphs[0]

group_name = '[название группы]'
p0.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.LEFT
p0.add_run('аудит группы ', style="WhiteBoldHeaderStyleFont")
p0.add_run(f"{group_name}", style="RedBoldHeaderStyleFont")
shading_elm = parse_xml(r'<w:shd {} w:fill="7000FF"/>'.format(nsdecls('w')))
table.cell(0, 0)._tc.get_or_add_tcPr().append(shading_elm)
set_cell_border(
        main_cell,
        top={"sz": 0, "color": "#FFFFFF", "val": "single", "space": "0"},
        bottom={"sz": 0, "color": "#FFFFFF", "val": "single", "space": "0"},
        start={"sz": 0, "color": "#FFFFFF", "val": "single", "space": "0"},
        end={"sz": 0, "color": "#FFFFFF", "val": "single", "space": "0"},
    )

p = create_paragraph(text='', style='Normal', before=12, after=6, prev=None, indent=1.2, align='LEFT')
hyperlink = add_hyperlink(p, 'https://vk.com/', 'https://vk.com/', 'FF0000', False)


p = create_paragraph(text='', style='Normal', before=6, after=6, prev=None, indent=1.2, align='JUSTIFY')
p.add_run('Принцип работы Adstetica*', style="BlackBoldStyleFont14")
p = create_paragraph(text='', style='Normal', before=6, after=12, prev=None, indent=1.2, align='LEFT')
p.add_run('Adstetica анализирует вашу целевую аудиторию по:', style="BlackBoldStyleFont")
analyzed_features = ["полу;",
                     "возрасту;",
                     "месту жительства;",
                     "семейному положению;",
                     "интересам, а именно - темам, о которых каждый пользователь в выборке пишет на своей странице;",
                     "изображениям, которые размещает;",
                     "типу текста (цель послания);",
                     "базовым эмоциям, присущим человеку;",
                     "базовому поведению;",
                     "тенденциям речи;",
                     "особенностям мышления;",
                     "особенностям принятия решений."]
for feature in analyzed_features:
    p = create_paragraph(text='', style='List Number', before=0, after=0, prev=None, indent=1.2, align='JUSTIFY')
    p.add_run(feature, style="BlackBoldStyleFont")


p = create_paragraph(text='', style='Normal', before=12, after=12, prev=None, indent=1.2, align='JUSTIFY')
p.add_run("""Этот детальный анализ позволяет выстроить коммуникацию с человеком или группой людей более эффективно: используя в скриптах продаж, в разработке контент-плана группы в социальных сетях, в формировании ключевого сообщения новой рекламной кампании.""", style="BlackBoldStyleFont")

count_of_users = 1000
p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=1.2, align='JUSTIFY')
p.add_run("Итак, анализ ", style='BlackStyleFont')
p.add_run(f"{count_of_users} ", style='RedStyleFont')
p.add_run("пользователей группы дает нам следующее представление об аудитории:", style='BlackStyleFont')

p = create_paragraph(text='', style='Normal', before=12, after=0, prev=None, indent=0, align='LEFT')
p.add_run("Интересующие аудиторию темы**:", style='PurpleStyleFont')

texts_themes = ['IT', 'Путешествия', 'Спорт']
for theme in texts_themes:
    p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=0, align='LEFT')
    p.add_run(theme, style='GreenStyleFont')


p = create_paragraph(text='', style='Normal', before=12, after=0, prev=None, indent=0, align='LEFT')
p.add_run("Доминирующий тип постов***:", style='PurpleStyleFont')

posts_themes = ['Новости', 'Цитаты', 'Руководство']
for theme in posts_themes:
    p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=0, align='LEFT')
    p.add_run(theme, style='GreenStyleFont')


gender_percentage = {'М': 56, 'Ж': 44}
p = create_paragraph(text='', style='Normal', before=12, after=0, prev=None, indent=0, align='LEFT')
p.add_run("Пол: ", style='PurpleStyleFont')
p.add_run(f" М - ", style='BlackStyleFont')
p.add_run(f"{gender_percentage['М']}%", style='GreenStyleFont')
p.add_run(", ", style='PurpleStyleFont')
p.add_run(f"Ж - ", style='BlackStyleFont')
p.add_run(f"{gender_percentage['Ж']}%", style='GreenStyleFont')

p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=0, align='LEFT')
p.add_run("Возраст:", style='PurpleStyleFont')


ages = {'15-20': 98,
        '20-25': 232,
        '25-30': 235,
        '30-35': 285,
        '35-40': 127,
        '40-45': 94,
        '45-50': 28,
        '50-55': 20,
        '55-60': 18,
        '60-65': 2,
        '65-70': 10,
        '70-75': 4,
        '75-80': 3,
        '80-85': 1}

for age_group, count in ages.items():
    p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=0, align='LEFT')
    p.add_run(age_group + ' ', style='PurpleStyleFont')
    p.add_run(str(count) + ',', style='GreenStyleFont')


location = {'Челябинск': 92, 'Москва': 5, 'Санкт-Петербург': 3}
p = create_paragraph(text='', style='Normal', before=12, after=0, prev=None, indent=0, align='LEFT')
p.add_run("Место жительства: ", style='PurpleStyleFont')
for loc, percent in location.items():
    p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=0, align='LEFT')
    p.add_run(f"{loc}", style='PurpleStyleFont')
    p.add_run(f" - {percent}%", style='GreenStyleFont')


result_psychotypes = {'First': 'тревожный', 'Second': 'эмотивный'}
p = create_paragraph(text='', style='Normal', before=12, after=0, prev=None, indent=0, align='LEFT')
p.add_run("Доминирующий психотип: ", style='PurpleStyleFont')
p.add_run(f"{result_psychotypes['First']}", style='GreenStyleFont')
p.add_run(", дополняющим является: ", style='BlackBoldStyleFont')
p.add_run(f"{result_psychotypes['Second']}", style='GreenStyleFont')
p.add_run(".", style='BlackStyleFont')

result_tendentions = {'First': 'Консервация жизненных состояний, от всего что связано с ответственностью и новизной, избежать энергозатрат.',
                      'Second': "Гуманизация и гармонизация внутреннего и внешнего мира людей во всех аспектах."}

p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=0, align='JUSTIFY')
p.add_run("Доминирующая тенденция: ", style='PurpleStyleFont')
p.add_run(f"{result_tendentions['First']}", style='BlueStyleFont')
p = create_paragraph(text='', style='Normal', before=0, after=12, prev=None, indent=0, align='JUSTIFY')
p.add_run("Вторичная тенденция: ", style='BlackBoldStyleFont')
p.add_run(f"{result_tendentions['Second']}", style='BlueStyleFont')

sell_recommendations = {'First': "Позаботьтесь о наличии подробной инструкции к вашему продукту."
                                 "Этот тип характера должен знать, что делает ваш продукт, как он работает"
                                 "и что с ним делать в случае, если что-то пойдёт не так. Нелишним будут"
                                 "доказательства безопасности и полной безотказности. Впрочем, быстрого решения"
                                 "ожидать все равно не стоит – как уже говорилось, психотип не склонен к"
                                 "импульсивным поступкам.",
                      'Second': "Учитывая вторичную тенденцию, уместно использование образов семьи, домашнего очага, "
                                "эмоционального контакта, близости, гармоничные картины природы."}


p = create_paragraph(text='', style='Normal', before=12, after=0, prev=None, indent=1, align='JUSTIFY')
p.add_run("Как ему продавать: ", style='PurpleStyleFont')
p.add_run(f"{sell_recommendations['First']}", style='BlueStyleFont')
p = create_paragraph(text='', style='Normal', before=0, after=0, prev=None, indent=1, align='JUSTIFY')
p.add_run(f"{sell_recommendations['Second']}", style='BlueStyleFont')

questions = ["",
             "Какая должна быть речь в коммуникации с таким типом клиентов?",
             "Какая должна быть картинка в коммуникации с таким типом клиентов?",
             "Какая должна быть общая эстетика коммуникации с таким типом клиентов?",
             "Какая должна быть модель коммуникации с таким типом клиентов?"
            ]

recommendations = ["Рекомендуем",
                   "Простая, понятная, эмоционально-нейтральная, возможно, даже формальная.",
                   "Нейтральные цвета, без ярких вставок, использование образов, символизирующих безопасность, понятность.",
                   "Дружелюбная, но не давящая. Интеллигентная, аккуратная, посыл, не нарушающий границ. Осторожное предложение с гарантией безопасности и контроля.",
                   "Безопасность, повторяемость, предсказуемость и процедурный контроль. Желательны указания гарантии и опора на семейные ценности. Экология"
                  ]

discommendations = ["Не рекомендуем",
                    "Неологизмы, иностранные слова, кричащую, избыточно эмоциональную лексику.",
                    "Яркие выносы, смеющиеся, кричащие люди, чрезмерная динамика, ломаная геометрия.",
                    "Давящие, яркие образ, острые углы, кислотные цвета.",
                    "Хаос, указание на новизну, инновационность и эксперименты."
                    ]

records = [(questions[i], recommendations[i], discommendations[i]) for i in range(len(questions))]
table = document.add_table(rows=0, cols=3)
for record in records:
    row_cells = table.add_row().cells
    for i in range(len(record)):
        row_cells[i].text = record[i]

p = create_paragraph(text='', style='Normal', before=24, after=6, prev=None, indent=0, align='LEFT')
p.add_run("Примечания:", style='PurpleBoldStyleFont14')
p = create_paragraph(text='', style='Normal', before=0, after=6, prev=None, indent=0, align='LEFT')
p.add_run("*Adstetica обучена на 100 000 пользовательских текстах и 60 000 пользовательских фотографий.", style='BlackBoldItalicStyleFont')
p = create_paragraph(text='', style='Normal', before=0, after=6, prev=None, indent=0, align='LEFT')
p.add_run("**Как расшифровываются темы текстов?", style='BlackBoldItalicStyleFont')

texts_themes_description =  {'IT': "Тексты, описывающие инновационные решения (обращаем внимание, "
                                   "что для типирования есть отдельно темы Data Science, Наука и техника, "
                                   "Бизнес, Видеоигры, то есть, тема IT их уже не включает).",
                              'Путешествия': "Тексты, описывающие передвижения по какой-либо территории или "
                                             "акватории с целью их изучения, а также с общеобразовательными, "
                                             "познавательными, развлекательными и другими целями.",
                              'Спорт': "Тексты, в которых описывается спорт, важные события из мира спорта, важные личности в мире спорта или спортивное снаряжение."}
level += 1
for i, (theme, description) in tuple(enumerate(texts_themes_description.items())):
    p = create_paragraph(text='', style='List Number', before=12, after=0, prev=None, indent=1.25, align='JUSTIFY')
    p.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    p.add_run(f"{theme} - ", style='BlackBoldStyleFont')
    p.add_run(description, style="BlueStyleFont")

p = create_paragraph(text='', style='Normal', before=12, after=0, prev=None, indent=0, align='LEFT')
p.add_run("**Как расшифровываются типы текстов?", style='BlackBoldItalicStyleFont')

texts_types_description = {'Руководство': "статьи с руководством по использованию, с инструкцией к применению.",
                            'Новости': "описание недавно случившихся событий, происшествий, открытий.",
                            'Цитаты': "цитаты великих людей."}
level += 1
for i, (type, description) in enumerate(texts_types_description.items()):
    p = create_paragraph(text='', style='List Number', before=12, after=0, prev=None, indent=1.25, align='JUSTIFY')
    p.add_run(f"{type} - ", style='BlackBoldStyleFont')
    p.add_run(description, style="BlueStyleFont")

document.add_page_break()
document.save(report_name)   # Save Adstetica_Report.docx


def convert_to_pdf(filename, timeout=None):
    args = ['lowriter', '-pt', 'pdf', filename]
    process = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
    filename = re.search('-> (.*?) using filter', process.stdout.decode())

convert_to_pdf(report_name, timeout=15)