import docx
from docx.shared import Pt
from docx.enum.style import WD_STYLE_TYPE
from docx.shared import RGBColor
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
# sudo apt-get install cups-pdf
# lowriter -pt pdf your_word_file.doc
# sudo apt-get install libreoffice-script-provider-python

font_size = 11
font_style = 'Montserratular'

def setup_styles(styles):
    '''Add Styles for report docx file'''
    obj_PurpleBoldStyle = styles.add_style('PurpleStyleFont', WD_STYLE_TYPE.CHARACTER)
    PurpleBoldStyleFont = obj_PurpleBoldStyle.font
    PurpleBoldStyleFont.size = Pt(font_size-1)
    PurpleBoldStyleFont.color.rgb = RGBColor(112, 0, 255)    # 7000FF # font.color.brightness = 0.4
    PurpleBoldStyleFont.name = font_style
    PurpleBoldStyleFont.bold = True

    obj_PurpleBoldStyle = styles.add_style('PurpleBoldStyleFont', WD_STYLE_TYPE.CHARACTER)
    PurpleBoldStyleFont = obj_PurpleBoldStyle.font
    PurpleBoldStyleFont.size = Pt(font_size)
    PurpleBoldStyleFont.color.rgb = RGBColor(112, 0, 255)    # 7000FF # font.color.brightness = 0.4
    PurpleBoldStyleFont.name = font_style
    PurpleBoldStyleFont.bold = True

    obj_PurpleBoldStyle14 = styles.add_style('PurpleBoldStyleFont14', WD_STYLE_TYPE.CHARACTER)
    PurpleBoldStyleFont14 = obj_PurpleBoldStyle14.font
    PurpleBoldStyleFont14.size = Pt(font_size+3)
    PurpleBoldStyleFont14.color.rgb = RGBColor(112, 0, 255)    # 7000FF # font.color.brightness = 0.4
    PurpleBoldStyleFont14.name = font_style
    PurpleBoldStyleFont14.bold = True
    PurpleBoldStyleFont14.italic = True

    obj_BlackStyle = styles.add_style('BlackStyleFont', WD_STYLE_TYPE.CHARACTER)
    BlackStyleFont = obj_BlackStyle.font
    BlackStyleFont.size = Pt(font_size)
    BlackStyleFont.color.rgb = RGBColor(0, 0, 0)    # Black Color
    BlackStyleFont.name = font_style
    BlackStyleFont.bold = False

    obj_BlackBoldStyle = styles.add_style('BlackBoldStyleFont', WD_STYLE_TYPE.CHARACTER)
    BlackBoldStyleFont = obj_BlackBoldStyle.font
    BlackBoldStyleFont.size = Pt(font_size)
    BlackBoldStyleFont.color.rgb = RGBColor(0, 0, 0)    # Black Color
    BlackBoldStyleFont.name = 'ProximaNova' # font_style
    BlackBoldStyleFont.bold = True

    obj_BlackBoldItalicStyle = styles.add_style('BlackBoldItalicStyleFont', WD_STYLE_TYPE.CHARACTER)
    BlackBoldItalicStyleFont = obj_BlackBoldItalicStyle.font
    BlackBoldItalicStyleFont.size = Pt(font_size)
    BlackBoldItalicStyleFont.color.rgb = RGBColor(0, 0, 0)    # Black Color
    BlackBoldItalicStyleFont.name = 'ProximaNova' # font_style
    BlackBoldItalicStyleFont.bold = True
    BlackBoldItalicStyleFont.italic = True

    BlackBoldStyleFont14 = styles.add_style('BlackBoldStyleFont14', WD_STYLE_TYPE.CHARACTER)
    BlackBoldStyleFont14 = BlackBoldStyleFont14.font
    BlackBoldStyleFont14.size = Pt(font_size+3)
    BlackBoldStyleFont14.color.rgb = RGBColor(0, 0, 0)    # Black Color
    BlackBoldStyleFont14.name = font_style
    BlackBoldStyleFont14.bold = True
    BlackBoldStyleFont14.italic = False

    obj_WhiteBoldHeaderStyle = styles.add_style('WhiteBoldHeaderStyleFont', WD_STYLE_TYPE.CHARACTER)
    WhiteBoldHeaderStyleFont = obj_WhiteBoldHeaderStyle.font
    WhiteBoldHeaderStyleFont.size = Pt(font_size+7)
    WhiteBoldHeaderStyleFont.color.rgb = RGBColor(255, 255, 255)    # Black Color
    WhiteBoldHeaderStyleFont.name = 'Montserrat-Regular' # font_style
    WhiteBoldHeaderStyleFont.bold = True
    #WhiteBoldHeaderStyleFont.highlight_color = WD_COLOR_INDEX.VIOLET    # RGBColor(112, 0, 255)

    obj_RedBoldHeaderStyle = styles.add_style('RedBoldHeaderStyleFont', WD_STYLE_TYPE.CHARACTER)
    RedBoldHeaderStyleFont = obj_RedBoldHeaderStyle.font
    RedBoldHeaderStyleFont.size = Pt(font_size+7)
    RedBoldHeaderStyleFont.color.rgb = RGBColor(255, 0, 0)    # Black Color
    RedBoldHeaderStyleFont.name = 'Montserratular' # font_style
    RedBoldHeaderStyleFont.bold = True
    #RedBoldHeaderStyleFont.highlight_color = WD_COLOR_INDEX.VIOLET    # RGBColor(112, 0, 255)

    obj_RedStyle = styles.add_style('RedStyleFont', WD_STYLE_TYPE.CHARACTER)
    RedStyleFont = obj_RedStyle.font
    RedStyleFont.size = Pt(font_size)
    RedStyleFont.color.rgb = RGBColor(255, 0, 0)    # Black Color
    RedStyleFont.name = font_style
    RedStyleFont.bold = False

    obj_GreenStyle = styles.add_style('GreenStyleFont', WD_STYLE_TYPE.CHARACTER)
    GreenStyleFont = obj_GreenStyle.font
    GreenStyleFont.size = Pt(font_size)
    GreenStyleFont.color.rgb = RGBColor(56, 118, 29)    # Black Color
    GreenStyleFont.name = font_style
    GreenStyleFont.bold = False

    obj_BlueStyle = styles.add_style('BlueStyleFont', WD_STYLE_TYPE.CHARACTER)
    BlueStyleFont = obj_BlueStyle.font
    BlueStyleFont.size = Pt(font_size)
    BlueStyleFont.color.rgb = RGBColor(0, 66, 255)    # Black Color
    BlueStyleFont.name = font_style
    BlueStyleFont.bold = False

def set_cell_border(cell, **kwargs):
    """
    Set cell`s border
    Usage:
    set_cell_border(
        cell,
        top={"sz": 12, "val": "single", "color": "#FF0000", "space": "0"},
        bottom={"sz": 12, "color": "#00FF00", "val": "single"},
        start={"sz": 24, "val": "dashed", "shadow": "true"},
        end={"sz": 12, "val": "dashed"},
    )
    """
    tc = cell._tc
    tcPr = tc.get_or_add_tcPr()
    # check for tag existnace, if none found, then create one
    tcBorders = tcPr.first_child_found_in("w:tcBorders")
    if tcBorders is None:
        tcBorders = OxmlElement('w:tcBorders')
        tcPr.append(tcBorders)
    # list over all available tags
    for edge in ('start', 'top', 'end', 'bottom', 'insideH', 'insideV'):
        edge_data = kwargs.get(edge)
        if edge_data:
            tag = 'w:{}'.format(edge)
            # check for tag existnace, if none found, then create one
            element = tcBorders.find(qn(tag))
            if element is None:
                element = OxmlElement(tag)
                tcBorders.append(element)
            # looks like order of attributes is important
            for key in ["sz", "val", "color", "space", "shadow"]:
                if key in edge_data:
                    element.set(qn('w:{}'.format(key)), str(edge_data[key]))


def add_hyperlink(paragraph, url, text, color, underline):
    """
    A function that places a hyperlink within a paragraph object.
    :param paragraph: The paragraph we are adding the hyperlink to.
    :param url: A string containing the required url
    :param text: The text displayed for the url
    :return: The hyperlink object
    """
    # This gets access to the document.xml.rels file and gets a new relation id value
    part = paragraph.part
    r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)
    # Create the w:hyperlink tag and add needed values
    hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
    hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )
    # Create a w:r element
    new_run = docx.oxml.shared.OxmlElement('w:r')
    # Create a new w:rPr element
    rPr = docx.oxml.shared.OxmlElement('w:rPr')
    # Add color if it is given
    if not color is None:
      c = docx.oxml.shared.OxmlElement('w:color')
      c.set(docx.oxml.shared.qn('w:val'), color)
      rPr.append(c)
    # Remove underlining if it is requested
    if not underline:
      u = docx.oxml.shared.OxmlElement('w:u')
      u.set(docx.oxml.shared.qn('w:val'), 'none')
      rPr.append(u)
    # Join all the xml elements together add add the required text to the w:r element
    new_run.append(rPr)
    new_run.text = text
    hyperlink.append(new_run)
    paragraph._p.append(hyperlink)
    return hyperlink


def list_number(doc, par, prev=None, level=None, num=True):
    """
    Makes a paragraph into a list item with a specific level and
    optional restart.

    An attempt will be made to retreive an abstract numbering style that
    corresponds to the style of the paragraph. If that is not possible,
    the default numbering or bullet style will be used based on the
    ``num`` parameter.

    Parameters
    ----------
    doc : docx.document.Document
        The document to add the list into.
    par : docx.paragraph.Paragraph
        The paragraph to turn into a list item.
    prev : docx.paragraph.Paragraph or None
        The previous paragraph in the list. If specified, the numbering
        and styles will be taken as a continuation of this paragraph.
        If omitted, a new numbering scheme will be started.
    level : int or None
        The level of the paragraph within the outline. If ``prev`` is
        set, defaults to the same level as in ``prev``. Otherwise,
        defaults to zero.
    num : bool
        If ``prev`` is :py:obj:`None` and the style of the paragraph
        does not correspond to an existing numbering style, this will
        determine wether or not the list will be numbered or bulleted.
        The result is not guaranteed, but is fairly safe for most Word
        templates.
    """
    xpath_options = {
        True: {'single': 'count(w:lvl)=1 and ', 'level': 0},
        False: {'single': '', 'level': level},
    }

    def style_xpath(prefer_single=True):
        """
        The style comes from the outer-scope variable ``par.style.name``.
        """
        style = par.style.style_id
        return (
            'w:abstractNum['
                '{single}w:lvl[@w:ilvl="{level}"]/w:pStyle[@w:val="{style}"]'
            ']/@w:abstractNumId'
        ).format(style=style, **xpath_options[prefer_single])

    def type_xpath(prefer_single=True):
        """
        The type is from the outer-scope variable ``num``.
        """
        type = 'decimal' if num else 'bullet'
        return (
            'w:abstractNum['
                '{single}w:lvl[@w:ilvl="{level}"]/w:numFmt[@w:val="{type}"]'
            ']/@w:abstractNumId'
        ).format(type=type, **xpath_options[prefer_single])

    def get_abstract_id():
        """
        Select as follows:

            1. Match single-level by style (get min ID)
            2. Match exact style and level (get min ID)
            3. Match single-level decimal/bullet types (get min ID)
            4. Match decimal/bullet in requested level (get min ID)
            3. 0
        """
        for fn in (style_xpath, type_xpath):
            for prefer_single in (True, False):
                xpath = fn(prefer_single)
                ids = numbering.xpath(xpath)
                if ids:
                    return min(int(x) for x in ids)
        return 0

    if (prev is None or
            prev._p.pPr is None or
            prev._p.pPr.numPr is None or
            prev._p.pPr.numPr.numId is None):
        if level is None:
            level = 0
        numbering = doc.part.numbering_part.numbering_definitions._numbering
        # Compute the abstract ID first by style, then by num
        anum = get_abstract_id()
        # Set the concrete numbering based on the abstract numbering ID
        num = numbering.add_num(anum)
        # Make sure to override the abstract continuation property
        num.add_lvlOverride(ilvl=level).add_startOverride(1)
        # Extract the newly-allocated concrete numbering ID
        num = num.numId
    else:
        if level is None:
            level = prev._p.pPr.numPr.ilvl.val
        # Get the previous concrete numbering ID
        num = prev._p.pPr.numPr.numId.val
    par._p.get_or_add_pPr().get_or_add_numPr().get_or_add_numId().val = num
    par._p.get_or_add_pPr().get_or_add_numPr().get_or_add_ilvl().val = level




'''
hdr_cells = table.rows[0].cells
hdr_cells[0].text = 'Qty'
hdr_cells[1].text = 'Id'
hdr_cells[2].text = 'Desc'
'''
#document.add_paragraph('Intense quote', style='Intense Quote')
#document.add_paragraph('first item in unordered list', style='List Bullet')
#document.add_paragraph('first item in ordered list', style='List Number')
